<!DOCTYPE html>
<html lang="en">
<head>
	<title>For Loops</title>
</head>
<body>
	<?php 
		$count = 0;
		while($count <= 10){
			if($count % 2 == 0){
				echo $count . " is even <br />";
			}else{
				echo $count . " is odd <br />";
			}
			$count ++;
		}
		echo "<br />";
		echo "Count: {$count}";
		
		echo "<br />";
		echo "<br />";
		
		for($count1 = 10; $count1 <= 20; $count1 ++){
			if($count1 % 2 == 0){
				echo $count1 . " is even <br />";
			}else{
				echo $count1 . " is odd <br />";
			}
		}
		echo "<br />";
		echo "Count: {$count1}";
	?>
</body>
</html>