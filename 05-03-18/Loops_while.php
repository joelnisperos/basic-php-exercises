<!DOCTYPE html>
<html lang="en">
<head>
	<title>While Loops</title>
</head>
<body>
	<?php 
		$count = 0;
		while($count <= 10){
			if($count % 2 == 0){
				echo $count . " is even <br />";
			}else{
				echo $count . " is odd <br />";
			}
			$count ++;
		}
		echo "<br />";
		echo "Count: {$count}";
	?>
</body>
</html>