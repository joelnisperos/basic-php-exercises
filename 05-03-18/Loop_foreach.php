<!DOCTYPE html>
<html lang="en">
<head>
	<title>Foreach Loops</title>
</head>
<body>
	<?php 
		$ages = [4,8,15,16,23,42];
		foreach($ages as $age){
			echo "Age: {$age}<br />";
		}
		
		echo "<br/>";
		
		$person = [
			"first_name" => "Joel",
			"last_name"  => "Nisperos",
			"address"   => "La Union",
			"gender"    => "Male",
			"age"       => "19"
		];
		foreach($person as $attribute => $data){
			$attr_nice = ucwords(str_replace("_", " ", $attribute));
			echo "{$attr_nice}: {$data}<br/>";
		}
		
		echo "<br/>";
		
		$prices = [
			"Brand New Computer" => 2000,
			"1 month of Lynda.com" => 25,
			"Learning PHP" => null
		];
		foreach($prices as $item => $price){
			echo "{$item}: ";
			if(is_int($price)){
				echo "$" . $price;
			}else{
				echo "priceless";
			}
			echo "<br/>";
		}
	?>
</body>
</html>