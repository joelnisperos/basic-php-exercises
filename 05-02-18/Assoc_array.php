<!DOCTYPE html>
<html lang="en">
<head>
	<title>Associative Array</title>
</head>
<body>
	<?php $assoc = ["firstname" => "Joel", "lastname" => "Nisperos"]?>
	<?php echo $assoc["firstname"]?><br />
	<?php echo $assoc["firstname"] . " " .$assoc["lastname"]?><br />
	
	<?php $assoc["firstname"] = "Dark"?>
	<?php echo $assoc["firstname"] . " " .$assoc["lastname"]?><br />
	
	<?php 
	$numbers = [1,2,3,4,5];
	$numbers = ["0" => 1, "1" => 2, "2" => 3, "4" => 5];
	echo $numbers[0];
	
	?>
	
</body>
</html>