<!DOCTYPE html>
<html lang="en">
<head>
	<title>Switch</title>
</head>
<body>
	<?php 
		$a = 4;
		
		switch ($a){
			case 0:  echo "a equals 0<br />";				 break;
			case 1:  echo "a equals 1<br />";				 break;
			case 2:  echo "a equals 2<br />";				 break;
			case 3:  echo "a equals 3<br />";				 break;
			case 4:  echo "a equals 4<br />";				 break;
			default: echo "a is not equal to 0, 1, 2, 3, 4"; break;
		}		
	?><br/>
	
	<?php 
		$year = 1998;
		switch (($year - 4) % 12){
			case  0: $zodiac = 'Rat';  	  break;
			case  2: $zodiac = 'Ox'; 	  break;
			case  3: $zodiac = 'Tiger';   break;
			case  4: $zodiac = 'Rabbit';  break;
			case  5: $zodiac = 'Dragon';  break;
			case  6: $zodiac = 'Snake';   break;
			case  7: $zodiac = 'Horse';   break;
			case  8: $zodiac = 'Goat';    break;
			case  9: $zodiac = 'Monkey';  break;
			case 10: $zodiac = 'Rooster'; break;
			case 11: $zodiac = 'Dog'; 	  break;
			case 12: $zodiac = 'Pig'; 	  break;
		}
		echo "{$year} is the year on the {$zodiac}.";
	?>
	<br />
	<br />
	<?php 
		$user_type = "customer";
		switch($user_type){
			case "student":
				echo "Welcome {$user_type}";
				break;
			case "press":
			case "customer":
				echo "Hello {$user_type}";
				break;
		}
	?>
</body>
</html>