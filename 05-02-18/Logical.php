<!DOCTYPE html>
<html lang="en">
<head>
	<title>Logical</title>
</head>
<body>
	<?php 
		$a = 3;
		$b = 4;
		if ($a < $b){
			echo "a is lesser than b";
		}
		elseif ($a > $b){
			echo "b is lesser than a";
		}
		else{
			echo "a and b is equal";
		}
		
	?><br />
	
	<?php 
		$newUser = true;
		if($newUser){
			echo "<h1>Welcome</h1>";
			echo "<p>We are glad you decided to join us.</p>";
		}
	?><br />
</body>
</html>