<?php 
    $errors = [];
    
    function fieldname_as_text($fieldname){
        $fieldname = str_replace("_"," ", $fieldname);
        $fieldname = ucfirst($fieldname);
        return $fieldname;
    }

    function has_presence($value){
        return isset($value) && $value !== "";
    }

    function validate_presences($required_fields){
        global $errors;
        foreach ($required_fields as $field) {
            $value = trim($_POST[$field]);
            if (!has_presence($value)){
                $errors["$field"] = fieldname_as_text($field) . " cant be blank";
            }
        }

    }

    //max lenght
    function has_max_lenght($value, $max){
        return strlen($value) <= $max;
    }

    function validate_max_lenght($fields_with_max_lengths){
        global $errors;
        foreach ($fields_with_max_lengths as $field => $max) {
            $value = trim($_POST[$field]);
            if(!has_max_lenght($value, $max)){
                $errors[$field] = fieldname_as_text($field) . " is too long";
            }
        }
    }
    // * inclusion set
    function has_inclusion_in($value, $set){
        return in_array($value, $set);
    }

    // if(!empty($errors)){
        //     redirect_to("First_Page.php");
        //     include("form.php");
        // }else{
        //     include("success.php");
        // }

        //finds out if the key exist in taht array
        // if($array_key_exist($errors, "name")){
        //     echo "<span class=\"error-field\">&lt;&lt;</span>";
        // }
?>