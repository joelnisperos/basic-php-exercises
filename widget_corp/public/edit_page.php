<?php require_once("../includes/session.php");?>
<?php require_once("../includes/conn.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/validation_function.php");?>
<?php 
    if (!isset($_SESSION['admin_id'])){
        redirect_to("login.php");
    }
?>
<?php find_selected_page();?>

<?php
    if(!$current_page){
        redirect_to("manage_content.php");
    }
?>
<?php
    if(isset($_POST['submit'])){
        
        $required_fields = ["menu_name", "position", "visible", "content"];
        validate_presences($required_fields);

        $fields_with_max_lenghts = ["menu_name" => 30];
        validate_max_lenght($fields_with_max_lenghts);
        
        if(empty($errors)){   
            $id = $current_page["id"];
            $menu_name = mysqli_prep($_POST["menu_name"]);
            $position =(int) $_POST["position"]; 
            $visible =(int) $_POST["visible"];
            $content = mysqli_prep($_POST["content"]);

            $query = "UPDATE pages SET menu_name = '$menu_name', position = '$position', visible = '$visible', content = '$content' WHERE id = '$id' LIMIT 1";
            $result = mysqli_query($connection, $query);

            if($result && mysqli_affected_rows($connection) == 1){
                $_SESSION["message"] = "Subject Updated!";
                $_SESSION["created_subject_tracker"] = true;
                redirect_to("manage_content.php");
            }else {
                $message = "Subject Update Failed!";
                $_SESSION["created_subject_tracker"] = true;
            }
        }

    } else{ 
    }
?>

<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php");?>
<div id="main">
    <div id="navigation">
        <?php echo navigation($current_subject, $current_page);?>
    </div>
    <div id="page">
        <?php 
            if(!empty($message)){
                echo "<div class\"message\">" . htmlientities($message) . "</div>";
            }
        ?>
        <?php echo form_errors($errors);?>

        <h2>Edit Page: <?php echo htmlentities($current_page["menu_name"]);?></h2>

        <form action="edit_page.php?page=<?php echo urlencode($current_page["id"]);?>" method="post">
            <p>Page Name:
                <input type="text" name="menu_name" value="<?php echo htmlentities($current_page["menu_name"]);?>" />
            </p>
            <p>Position:
                <select name="position">
                <?php
                    $page_set = find_pages_for_subjects($current_page["subject_id"], false);
                    $page_count = mysqli_num_rows($page_set);
                    for($count=1; $count <= $page_count; $count ++){
                        echo "<option value=\"{$count}\"";
                        if ($current_page["position"] == $count){
                            echo " selected";
                        }
                        echo ">{$count}</option>";
                    }
                ?>
                </select>
            </p>

            <p>Visible:
                <input type="radio" name="visible" value="0" <?php if($current_page["visible"] == "0"){ echo "checked";} ?> /> No
                &nbsp;
                <input type="radio" name="visible" value="1" <?php if($current_page["visible"] == "1"){ echo "checked";} ?> /> Yes
            </p>

            <p>Content: <br/>
                <textarea name="content" rows="20" cols="80"><?php echo htmlentities($current_page["content"]);?></textarea>
            </p>

            <input type="submit" name="submit" value="Create Page" />
        </form>
        <br />

        <a href="manage_content.php?page=<?php urlencode($current_page["id"]);?>">Cancel</a>
        &nbsp;
        &nbsp;
        <a href="delete_page.php?page=<?php echo $current_page["id"];?>" onclick="return confirm('Are you sure you want to DELETE this Page: <?php echo $current_page["menu_name"];?>?');">Delete Page</a>
    </div>
</div>
<?php include("../includes/layouts/footer.php");?>