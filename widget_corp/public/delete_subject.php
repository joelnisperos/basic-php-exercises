<?php require_once("../includes/session.php");?>
<?php require_once("../includes/conn.php");?>
<?php require_once("../includes/functions.php");?>
<?php 
    if (!isset($_SESSION['admin_id'])){
        redirect_to("login.php");
    }
?>
<?php find_selected_page();?>

<?php
    $current_subject = find_subject_by_id($_GET["subject"]);
    if(!$current_subject){
        redirect_to("manage_content.php");
    }

    $pages_set = find_pages_for_subjects($current_subject["id"]);
    if(mysqli_num_rows($pages_set) > 0){
        $_SESSION["message"] = "Cant Delete subject with pages!";
        $_SESSION["created_subject_tracker"] = true;
        redirect_to("manage_content.php?subject={$current_subject["id"]}");
    }

    $id = $current_subject["id"];
    $query = "DELETE FROM subjects WHERE id = '{$id}' LIMIT 1";
    $result = mysqli_query($connection, $query);

    if($result && mysqli_affected_rows($connection) == 1){
        $_SESSION["message"] = "Subject Deleted!";
        $_SESSION["created_subject_tracker"] = true;
        redirect_to("manage_content.php");
    }else {
        $_SESSION["message"] = "Subject Deletion Failed!";
        $_SESSION["created_subject_tracker"] = true;
        redirect_to("manage_content.php?subject={$id}");
    }
?>