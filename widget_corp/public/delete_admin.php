<?php require_once("../includes/session.php");?>
<?php require_once("../includes/conn.php");?>
<?php require_once("../includes/functions.php");?>
<?php 
    if (!isset($_SESSION['admin_id'])){
        redirect_to("login.php");
    }
?>
<?php
    $admin = find_admin_by_id($_GET["id"], false);
    if(!$admin){
        redirect_to("manage_admins.php");
    }

    $id = $admin["id"];
    $query = "DELETE FROM admins WHERE id = '{$id}' LIMIT 1";
    $result = mysqli_query($connection, $query);

    if($result && mysqli_affected_rows($connection) == 1){
        $_SESSION["message"] = "Admin Deleted!";
        $_SESSION["created_subject_tracker"] = true;
        redirect_to("manage_admins.php");
    }else {
        $_SESSION["message"] = "Admin Deletion Failed!";
        $_SESSION["created_subject_tracker"] = true;
        redirect_to("manage_admins.php?id={$id}");
    }
?>