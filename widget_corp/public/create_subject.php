<?php require_once("../includes/session.php");?>
<?php require_once("../includes/conn.php");?>
<?php require_once("../includes/functions.php");?>
<?php 
    if (!isset($_SESSION['admin_id'])){
        redirect_to("login.php");
    }
?>
<?php require_once("../includes/validation_function.php");?>

<?php
    if(isset($_POST['submit'])){
        $menu_name = mysqli_prep($_POST["menu_name"]);
        $position =(int) $_POST["position"]; 
        $visible =(int) $_POST["visible"];
        
        $required_fields = ["menu_name", "position", "visible"];
        validate_presences($required_fields);

        $fields_with_max_lenghts = ["menu_name" => 30];
        validate_max_lenght($fields_with_max_lenghts);
        
        if(!empty($errors)){
            $_SESSION["errors"] = $errors;
            redirect_to("new_subject.php");
        }
        $query = "INSERT INTO `subjects`(`menu_name`, `position`, `visible`) VALUES ('$menu_name','$position','$visible')";
        $result = mysqli_query($connection, $query);

        if($result){
            $_SESSION["message"] = "Subject Created!";
            $_SESSION["created_subject_tracker"] = true;
            redirect_to("manage_content.php");
        }else {
            $_SESSION["message"] = "Subject Creation Failed!";
            $_SESSION["created_subject_tracker"] = false;
            redirect_to("new_subject.php");
        }
    } else{
        redirect_to("new_subject.php");
    }
?>

<?php if(isset($connection)){ mysqli_close($connection); }?>