<?php require_once("../includes/session.php");?>
<?php require_once("../includes/conn.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/validation_function.php");?>
<?php 
    if (!isset($_SESSION['admin_id'])){
        redirect_to("login.php");
    }
?>
<?php find_selected_page();?>

<?php 
    if (!$current_subject){
        redirect_to("manage_content.php");
    }
?>

<?php
    if(isset($_POST['submit'])){

        $required_fields = ["menu_name", "position", "visible", "content"];
        validate_presences($required_fields);

        $fields_with_max_lenghts = ["menu_name" => 30];
        validate_max_lenght($fields_with_max_lenghts);

        if(empty($errors)){
            $subject_id = $current_subject["id"];
            $menu_name = mysqli_prep($_POST["menu_name"]);
            $position =(int) $_POST["position"]; 
            $visible =(int) $_POST["visible"];
            $content = mysqli_prep($_POST["content"]);
            
            $query = "INSERT INTO `pages`(`subject_id`, `menu_name`, `position`, `visible`, `content`) VALUES ($subject_id, '$menu_name',$position,$visible,'$content')";
            $result = mysqli_query($connection, $query);

            if($result){
                $_SESSION["message"] = "Page Created!";
                $_SESSION["created_subject_tracker"] = true;
                redirect_to("manage_content.php?subject=".urlencode($current_subject["id"]));
            }else {
                $_SESSION["message"] = "Page Creation Failed!";
                $_SESSION["created_subject_tracker"] = true;
            }
        }    
    } else{
        
    }
?> 
<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php");?>
<div id="main">
    <div id="navigation">
        <?php echo navigation($current_subject, $current_page);?>
    </div>
    <div id="page">
        <?php 
        // if(isset($_SESSION["created_subject_tracker"])){
        //         echo message();
        //         $_SESSION["created_subject_tracker"] = false;
        //      } ?>
        <?php $errors = errors(); ?>
        <?php echo form_errors($errors);?>

        <h2>Create Page</h2>

        <form action="new_page.php?subject=<?php echo urlencode($current_subject["id"]);?>" method="post">
            <p>Page Name:
                <input type="text" name="menu_name" value="" />
            </p>
            <p>Position:
                <select name="position">
                <?php
                    $page_set = find_pages_for_subjects($current_subject["id"], false);
                    $page_count = mysqli_num_rows($page_set);
                    for($count=1; $count <= ($page_count + 1); $count ++){
                        echo "<option value=\"{$count}\">{$count}</option>";
                    }
                ?>
            </p>
                </select>
            <p>Visible:
                <input type="radio" name="visible" value="0" /> No
                &nbsp;
                <input type="radio" name="visible" value="1" /> Yes
            </p>
            <p>Content: <br/>
                <textarea name="content" rows="20" cols="80"></textarea>
            </p>

            <input type="submit" name="submit" value="Create Page" />
        </form>
        <br />
        <a href="manage_content.php">Cancel</a>
    </div>
</div>
<?php include("../includes/layouts/footer.php");?>