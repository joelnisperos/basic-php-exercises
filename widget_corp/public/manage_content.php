<?php require_once("../includes/session.php");?>
<?php require_once("../includes/conn.php");?>
<?php require_once("../includes/functions.php");?>

<?php 
    if (!isset($_SESSION['admin_id'])){
        redirect_to("login.php");
    }
?>
<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php");?>
<?php find_selected_page(true);?>

<div id="main">
    <div id="navigation">
        <br />
        <a href="admin.php">&laquo; Main Menu</a><br />
        <?php echo navigation($current_subject, $current_page);?>
        <br />
        <a href="new_subject.php">+ Add New Subject</a>
        <br/>
        <br/>
        <a href="logout.php" onclick="return confirm('Are you sure you want to LOGOUT?')">Logout</a>
    </div>

    <div id="page">
        <?php
            if(isset($_SESSION["created_subject_tracker"]) && $_SESSION["created_subject_tracker"]){
                echo message();
                $_SESSION["created_subject_tracker"] = false;
             }?>
        <?php if($current_subject){ ?>
                <h2>Manage Subject</h2>

                Menu Name: <?php echo htmlentities($current_subject["menu_name"]);?><br/>
                Position: <?php echo $current_subject["position"];?><br/>
                Visible: <?php echo $current_subject["visible"] == 1 ? 'Yes' : 'No';?><br/>

                <a href="edit_subject.php?subject=<?php echo urlencode($current_subject["id"]); ?>">Edit Subject</a>
                
                <div style="margin-top: 2em; border-top: 1px solid #000000;">
                    <h3>Pages in this Subject</h3>
                    <ul>
                        <?php
                            $subject_pages = find_pages_for_subjects($current_subject["id"], false);
                            while($page = mysqli_fetch_assoc($subject_pages)){
                                echo "<li>";
                                $safe_page_id = urlencode($page["id"]);
                                echo "<a href=\"manage_content.php?page={$safe_page_id}\">";
                                echo htmlentities($page["menu_name"]);
                                echo "</a>";
                                echo "</li>";
                            }
                        ?>
                    </ul>
                    <br />
                    +<a href="new_page.php?subject=<?php echo urlencode($current_subject["id"]);?>">A new page to this Subject</a>
                </div>
            <?php } elseif($current_page){ ?>
                <h2>Manage Page</h2>

                Menu Page: <?php echo htmlentities($current_page["menu_name"]);?><br/>
                Position: <?php echo $current_page["position"];?><br/>
                Visible: <?php echo $current_page["visible"] == 1 ? 'Yes' : 'No';?><br/>
                Content: <br />

                <div class="view-content">
                    <?php echo htmlentities($current_page["content"]);?>
                </div>

                <br/>
                <br/>

                <a href="edit_page.php?page=<?php echo urlencode($current_page['id']);?>">Edit Page</a>
        <?php
            } else{
        ?> Please select a subject or a page
        <?php } ?>
        
    </div>
</div>
<?php include("../includes/layouts/footer.php");?>