<?php require_once("../includes/session.php");?>
<?php require_once("../includes/conn.php");?>
<?php require_once("../includes/functions.php");?>
<?php 
    if (!isset($_SESSION['admin_id'])){
        redirect_to("login.php");
    }
?>
<?php find_selected_page();?>

<?php
    $current_page = find_pages_by_id($_GET["page"], false);
    if(!$current_page){
        redirect_to("manage_content.php");
    }

    $id = $current_page["id"];
    $query = "DELETE FROM pages WHERE id = '{$id}' LIMIT 1";
    $result = mysqli_query($connection, $query);

    if($result && mysqli_affected_rows($connection) == 1){
        $_SESSION["message"] = "Subject Deleted!";
        $_SESSION["created_subject_tracker"] = true;
        redirect_to("manage_content.php");
    }else {
        $_SESSION["message"] = "Subject Deletion Failed!";
        $_SESSION["created_subject_tracker"] = true;
        redirect_to("manage_content.php?subject={$id}");
    }
?>