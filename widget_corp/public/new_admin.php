<?php require_once("../includes/session.php");?>
<?php require_once("../includes/conn.php");?>
<?php require_once("../includes/functions.php");?>
<?php require_once("../includes/validation_function.php");?>

<?php
    if(isset($_POST['submit'])){

        $required_fields = ["username", "password"];
        validate_presences($required_fields);

        $fields_with_max_lenghts = ["username" => 30];
        validate_max_lenght($fields_with_max_lenghts);

        if(empty($errors)){
            $username = mysqli_prep($_POST["username"]);
            $password = encrypt_password($_POST["password"]);
            
            $query = "INSERT INTO `admins`(`username`, `hashed_password`) VALUES ('$username', '$password')";
            $result = mysqli_query($connection, $query);

            if($result){
                $_SESSION["message"] = "Admin Created!";
                $_SESSION["created_subject_tracker"] = true;
                redirect_to("manage_admins.php");
            }else {
                $_SESSION["message"] = "Admin Creation Failed!";
                $_SESSION["created_subject_tracker"] = false;
                redirect_to("new_admin.php");
            }
        }    
    } else{
        
    }
?> 
<?php $layout_context = "admin"; ?>
<?php include("../includes/layouts/header.php");?>

<div id="main">
    <div id="navigation">
        <br />
        <a href="admin.php">&laquo; Main Menu</a><br />
    </div>

    <div id="page">
        <?php
            if(isset($_SESSION["created_subject_tracker"]) && $_SESSION["created_subject_tracker"]){
                echo message();
                $_SESSION["created_subject_tracker"] = false;
            }
        ?>
        <h2>Create Admin</h2>
        <form action="new_admin.php" method="post">
            <p>Username:
                <input type="text" name="username" value="" />
            </p>
            <p>Password:
                <input type="password" name="password" value="" />
            </p>
            
            <input type="submit" name="submit" value="Create Admin" />
        </form>
        <br />
        <a href="manage_admin.php">Cancel</a>
    </div>
</div>
<?php include("../includes/layouts/footer.php");?>