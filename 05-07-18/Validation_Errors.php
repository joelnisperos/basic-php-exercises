<!DOCTYPE html>
<html lang="en">
<head>
	<title>Validation</title>
</head>
<body>
    <?php 
        require_once('Validation_Function.php');
        $errors = [];

        //$username = trim($_POST["username"]);
        $username = trim("");

        if (!has_presence($username)){
            $errors['username'] = "Username can't be blank";
        }

        echo form_errors($errors);
	?>
</body>
</html>