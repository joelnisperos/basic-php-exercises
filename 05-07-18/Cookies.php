<?php
    $name = "test";
    $value = "hello";
    $expire = time() + (60*60*24*7); // add seconds
    setcookie($name, $value, $expire);
    //setcookie($name, null, time() - 3600); //unsetting cookie
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Cookies</title>
</head>
<body>
    <pre>
    <?php
        $test = isset($_COOKIE["test"]) ? $_COOKIE["test"] : "";
        echo $test;
    ?>
    </pre>
</body>
</html>