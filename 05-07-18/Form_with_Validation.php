<?php
    require_once("Included_Function.php");
    require_once("Validation_Function.php");

    $errors = [];
    $message = "";

    if (isset($_POST['submit'])){
        $username = trim($_POST["username"]);
        $password = trim($_POST["password"]);
        
        // validations
        $fields_required = ["username", "password"];
        foreach($fields_required as $field){
            $value = trim($_POST[$field]);
            if (!has_presence($value)){
                $errors[$field] = ucfirst($field) ." can't be blank";
            }
        }
        // using assoc array
        $fields_with_max_lengths = ["username" => 30, "password" => 8];
        validate_max_lenght($fields_with_max_lengths);

        // login
        if( empty($errors) ){
            if($username == "demo" && $password == "demo"){
                redirect_to("Redirect_here.php");
            }else{
                $message = "Username Password do not match";
            }
        }
        } else {
        $username = "";
        $message = "Please Log in.";
        }
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Form with Validation</title>
</head>
<body>
    <?php 
        echo $message; 
    ?><br />
    <?php 
        echo form_errors($errors);
    ?>
	<form action="Form_with_Validation.php" method="post" >
        Username: <input type="text" name="username" value="<?php echo htmlspecialchars($username);?>" /><br/>
        Password: <input type="password" name="password" value=""/><br/>
        <br/>
        <input type="submit" name="submit" value="submit"/>
    </form>
    
</body>
</html>