<?php
    //sessions use cookies which use headers
    // Must start before any HTML output
    // unless buffering is turned on
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sessions</title>
</head>
<body>
    <?php
        $_SESSION["firstname"] = "Joel";
        $name = $_SESSION["firstname"];
        echo $name;
    ?>
</body>
</html>