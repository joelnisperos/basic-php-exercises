<?php 
    $errors = [];
    // *presence
    // uses trim() so empty space don't count
    // uses === to avoid false positive
    // empty() would consider "0" to be empty
    function has_presence($value){
        return isset($value) && $value !== "";
    }
    //max lenght
    function has_max_lenght($value, $max){
        return strlen($value) <= $max;
    }

    function validate_max_lenght($fields_with_max_lengths){
        global $errors;
        foreach ($fields_with_max_lengths as $field => $max) {
            $value = trim($_POST[$field]);
            if(!has_max_lenght($value, $max)){
                $errors[$field] = ucfirst($field) . " is too long";
            }
        }
    }
    // * inclusion set
    function has_inclusion_in($value, $set){
        return in_array($value, $set);
    }

    function form_errors($errors = []){
        $output = "";
        if(!empty($errors)){
            $output .= "<div class=\"error\">";
            $output .= "Please fix the following errors";
            $output .= "<ul>";
            foreach ($errors as $key => $error){
                $output .= "<li>{$error}</li>";
            }
            $output .= "</ul>";
            $output .= "</div>";
        }
        return $output;
    }
    // if(!empty($errors)){
        //     redirect_to("First_Page.php");
        //     include("form.php");
        // }else{
        //     include("success.php");
        // }

        //finds out if the key exist in taht array
        // if($array_key_exist($errors, "name")){
        //     echo "<span class=\"error-field\">&lt;&lt;</span>";
        // }
?>