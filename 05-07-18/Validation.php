<!DOCTYPE html>
<html lang="en">
<head>
	<title>Validation</title>
</head>
<body>
	<?php 
        // *presence
        // uses trim() so empty space don't count
        // uses === to avoid false positive
        // empty() would consider "0" to be empty
        $value = trim("0");
        if(!isset($value) || $value === ""){
            echo "validation Failed. <br />";
        }

        // min
        $value = "asd";
        $min = 3;
        if(strlen($value) > $min){
            echo "Validation Failed! <br />";
        }

        //max lenght
        $max = 6;
        if(strlen($value) > $max){
            echo "Validation Failed! <br />";
        }
        
        //type
        $value = "1";
        if(!is_string($value)){
            echo "Validation Failed! <br />";
        }

        // * inclusion set
        $value = "5";
        $set = ["1", "2", "3", "4", "5"];
        if(!in_array($value, $set)){
            echo "Validation Failed! <br />";
        }

        // * uniqueness 
        //uses a database to check uniqueness

        // * format
        // use a regex on a string
        //preg_match($regex, $subject);
        if(preg_match("/PHP/","PHP is fun.")){
            echo "A Match was Found!";
        } else {
            echo "A Match was not Found!";
        }

        $value = "nobody@nowhere.com";
        if(!preg_match("/@/", $value)){
            echo "Validation Failed! <br />";
        }
	?>
</body>
</html>