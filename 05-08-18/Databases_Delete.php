<?php
    // (1) create a database connection
    $dbhost = "localhost";
    $dbuser = "widget_cms";
    $dbpass = "negative28";
    $dbname = "widget_corp";
    $connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    //test if connection occured
    if(mysqli_connect_errno()){
        die("database connection failed: " . mysqli_connect_error() . " (" . mysqli_connect_errno() . " )"); 
    }
?>

<?php
    // often these are form values in $_POST
    $id = 6;
    
    // (2) perform database query
    $query = "DELETE FROM subjects WHERE id = {$id} LIMIT 1" ;
    
    $result = mysqli_query($connection, $query);

    //test if there was a query errors
    if($result && mysqli_affected_rows($connection) == 1){
        //success
        echo "Success!";
    }else {
        die("Database query failed! " . $query . mysqli_error($connection));
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Databases UPDATE</title>
</head>
<body>
    
</body>
</html>

<?php
    // (5) close database
    mysqli_close($connection);
?>