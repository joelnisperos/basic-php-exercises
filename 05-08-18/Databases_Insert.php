<?php
    // (1) create a database connection
    $dbhost = "localhost";
    $dbuser = "widget_cms";
    $dbpass = "negative28";
    $dbname = "widget_corp";
    $connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    //test if connection occured
    if(mysqli_connect_errno()){
        die("database connection failed: " . mysqli_connect_error() . " (" . mysqli_connect_errno() . " )"); 
    }
?>

<?php
    // often these are form values in $_POST
    $menu_name = "Today's Widget trivia";
    $position = 4;
    $visible = 1;

    // escape all string
    $menu_name = mysqli_real_escape_string($connection, $menu_name);
    
    // (2) perform database query
    $query = "INSERT INTO subjects (menu_name, position, visible)
              VALUES ('{$menu_name}', '{$position}', '{$visible}')";
    $result = mysqli_query($connection, $query);

    //test if there was a query errors
    if($result){
        //success
        echo "Success!";
    }else {
        die("Database query failed" . mysqli_error($connection));
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Databases INSERT</title>
</head>
<body>
    
</body>
</html>

<?php
    // (5) close database
    mysqli_close($connection);
?>