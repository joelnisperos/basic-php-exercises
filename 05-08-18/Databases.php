<?php
    // (1) create a database connection
    $dbhost = "localhost";
    $dbuser = "widget_cms";
    $dbpass = "negative28";
    $dbname = "widget_corp";
    $connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);
    //test if connection occured
    if(mysqli_connect_errno()){
        die("database connection failed: " . mysqli_connect_error() . " (" . mysqli_connect_errno() . " )"); 
    }
?>

<?php
    // (2) perform database query
    $query = "SELECT * FROM subjects WHERE visible = 1 ORDER BY position ASC";
    $result = mysqli_query($connection, $query);

    //test if there was a query errors
    if(!$result){
        die("database query failed");
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Databases</title>
</head>
<body>
    <ul>
    <?php
        // (3) return data
        while ($subject = mysqli_fetch_assoc($result)){
    ?>
        <li>
            <?php
                echo $subject["menu_name"] . " (" . $subject["id"] . ")";
            ?>
        </li>
    <?php
        }
    ?>
    </ul>
    <?php
        // (4) Release returned data
        mysqli_free_result($result);
    ?>
</body>
</html>

<?php
    // (5) close database
    mysqli_close($connection);
?>