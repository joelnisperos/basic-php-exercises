<?php
    session_start();
    if (!$_SESSION['member_id'])  
    {  
        header('location: /php_tutorials/05-09-18/index.php');  
        exit;  
    }
    $id = $_SESSION['member_id'];
    require_once("connection.php");
    $query = "SELECT * FROM members WHERE member_id = '$id'";
    $result = mysqli_query($connection, $query);
    $user = mysqli_fetch_assoc($result);
    $name = $user["member_name"];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home Page</title>
        <link rel="stylesheet" type="text/css" href="mystyle.css">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>

    <body>
        <div class="container-fluid">
            <div class="container">
                <h1>Home Page</h1>
                <p>Welcome <b><?php echo $name;?></b>.</p> 
                <a class="logout" href="logout.php">Log out</a>
                    <h2>Add Subject</h2>
                    <form class="form-fillup" action="Insert_Subject.php" method="post" id="frmfillup">
                        <input name="menu_name" type="text" value="" class="form-control" placeholder="Menu Name" required />
                        <input name="position" type="text" value="" class="form-control" placeholder="Position" required />
                        <input name="visible" type="text" value="" class="form-control" placeholder="Visible" required />
                        <button type="submit" name="AddSubj" value="AddSubjLogin" class="btn btn-lg btn-primary btn-block btn-signin">
                            Add Subject
                        </button>
                </form>
            </div>
        </div>
    </body>
</html>