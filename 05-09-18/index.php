<?php
	session_start();
	
	if(!empty($_POST["login"])) {
		try{
			require_once("connection.php");
			$a = $_POST["member_name"];
			$b = $_POST["member_password"];
			$query = "SELECT * FROM members WHERE member_email = '$a' AND member_password = '$b'";
			$result = mysqli_query($connection, $query);
			$user = mysqli_fetch_assoc($result);
		} catch (Exepttion $e){
			die("database connection failed: " . mysqli_connect_error() . " (" . mysqli_connect_errno() . " )"); 
		}
		if($user) {
				$_SESSION["member_id"] = $user["member_id"];
				
				if(!empty($_POST["remember"])) {
					setcookie ("member_login",$_POST["member_name"],time()+ (10 * 365 * 24 * 60 * 60));
					setcookie ("member_password",$_POST["member_password"],time()+ (10 * 365 * 24 * 60 * 60));
				} else {
					if(isset($_COOKIE["member_login"])) {
						setcookie ("member_login","");
					}
					if(isset($_COOKIE["member_password"])) {
						setcookie ("member_password","");
					}
				}
		} else {
			$message = "Username and Password do not match! ";
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login Page</title>
    <link rel="stylesheet" type="text/css" href="mystyle.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="card card-container">
			<img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
			<?php if(empty($_SESSION["member_id"])) { ?>
				<form class="form-signin" action="index.php" method="post" id="frmLogin">
					<div class="error-message">
						<?php if(isset($message)) { echo $message; } ?>
					</div>	
					
					<input name="member_name" type="text" value="<?php if(isset($_COOKIE["member_login"])) { echo $_COOKIE["member_login"]; } ?>"class="form-control" placeholder="Username" required autofocus >
					<input name="member_password" type="password" value="<?php if(isset($_COOKIE["member_password"])) { echo $_COOKIE["member_password"]; } ?>"class="form-control" placeholder="Password" required> 
				
					<div id="remember" class="checkbox">
						<label>
							<input type="checkbox" name="remember" id="remember" <?php if(isset($_COOKIE["member_login"])) { ?> checked <?php } ?> />Remember me
						</label>
					</div>

					<div>
						<button type="submit" name="login" value="Login" class="btn btn-lg btn-primary btn-block btn-signin">
							Sign In
						</button>
					</div>
				      
				</form>
				<?php } else { ?>
					<?php header("Location: home_page.php" );?>
			<?php } ?>
		</div>
	</div>
</body>
</html>