<!DOCTYPE html>
<html lang="en">
<head>
	<title>Define Function</title>
</head>
<body>
	<?php 
		function say_hello(){
            echo "Hello World!!<br />";
        }
        say_hello();

        function say_hello_to($word){
            echo "Hello {$word}! <br />";
        }
        say_hello_to("World");
        say_hello_to("Everyone");

        function say_hello_loudly(){
            echo "HELLO WORLD";
        }
        say_hello_loudly();
        //can't redefine functions
	?>
</body>
</html>