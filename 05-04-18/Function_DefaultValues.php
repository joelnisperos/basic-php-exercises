<!DOCTYPE html>
<html lang="en">
<head>
	<title>Function: Default Arguments Values</title>
</head>
<body>
	<?php 
        function paint($room="OFFICE",$color="RED"){
            return "The color is of the {$room} is {$color} <br />";
        }
        echo paint();
        echo paint("BEDROOM", "BLUE");
        echo paint("BEDROOM", null);
        echo paint("BEDROOM");
        echo paint("BLUE");
	?>
</body>
</html>