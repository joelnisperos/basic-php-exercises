<!DOCTYPE html>
<html lang="en">
<head>
	<title>HTML Encoding</title>
</head>
<body>
    <?php
        $link_text = "<Click> & learn More";
    ?>

    <a href="">
        <?php echo htmlspecialchars($link_text);?>
    </a>

    <br />

    <?php
        $text = "©®Þ";
        echo htmlentities($text);
    ?>

    <br/>

    <?php
        $url_page = "php/created/page/url.php";
        $param1 = "This is a string with <>";
        $param2 = "&#?*[]+ are bad characters";
        $linktext = "<Click> & learn More";

        $url = "http//localhost/";
        $url .= rawurlencode($url_page);
        $url .= "?" . "$param1=" . urlencode($param1);
        $url .= "?" . "$param2=" . urlencode($param2);
    ?>

    <a href="<?php echo htmlspecialchars($url);?>">
        <?php echo htmlspecialchars($linktext);?>
    </a>
</body>
</html>