<!DOCTYPE html>
<html lang="en">
<head>
	<title>Function: Global Scope</title>
</head>
<body>
	<?php 
        $bar = "Unchanged";
        function foo(){
            global $bar;
            if(isset($bar)){
                echo "foo: " . $bar . "<br/>";
            }
            $bar = "Change";
        }
        echo "1: " . $bar . "<br />";
        foo(); 
        echo "2: " . $bar . "<br />";
	?>
</body>
</html>