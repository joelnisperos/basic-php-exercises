<!DOCTYPE html>
<html lang="en">
<head>
	<title>Function: Return Values</title>
</head>
<body>
	<?php 
        function add($val1, $val2){
            $sum = $val1 + $val2;
            return $sum;
        }
        $result = add(4,5);
        $result1 = add(4, $result);
        echo $result1 . "<br />";

        function chinese_zodiac($year){
            switch (($year - 4) % 12){
                case  0: return 'Rat';
                case  2: return 'Ox';
                case  3: return 'Tiger';
                case  4: return 'Rabbit';
                case  5: return 'Dragon';
                case  6: return 'Snake';
                case  7: return 'Horse';
                case  8: return 'Goat';
                case  9: return 'Monkey';
                case 10: return 'Rooster';
                case 11: return 'Dog';
                case 12: return 'Pig';
            }
        }

        $zodiac = chinese_zodiac(1998);
        echo "1998 is the year of the {$zodiac}. <br />";

        echo "1997 is the year of the " . chinese_zodiac(2000) . ". <br />"; 

        //its a good practice to have return inside a function rather than echo
        function better_hello($greetings, $target, $punct){
            return $greetings . " " . $target . " " . $punct . "<br />";
        }
        echo better_hello("Hey", "Dark" , "!");
	?>
</body>
</html>