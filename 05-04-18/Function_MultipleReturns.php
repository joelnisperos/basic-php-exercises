<!DOCTYPE html>
<html lang="en">
<head>
	<title>Function: Multiple Return</title>
</head>
<body>
	<?php 
        function add($val1, $val2){
            $sum = $val1 + $val2;
            $subt = $val1 - $val2;
            return [$sum, $subt];
        }
        $result = add(5,4);
        echo "Sum: " . $result[0] . "<br />";
        echo "Difference: " . $result[1] . "<br />";

        echo "<br />";

        list($add, $dif) = add(10,4);
        echo "Sum: " . $add . "<br />";
        echo "Difference: " . $dif . "<br />";

        var_dump($result)
	?>
</body>
</html>