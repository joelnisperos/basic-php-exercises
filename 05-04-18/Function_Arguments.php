<!DOCTYPE html>
<html lang="en">
<head>
	<title>Function: Arguments</title>
</head>
<body>
	<?php 
        function say_hello_to($word){
            echo "Hello {$word}! <br />";
        }
        $name = "Joel Nisperos";
        say_hello_to($name);

        function better_hello($greetings, $target, $punct){
            echo $greetings . " " . $target . " " . $punct . "<br />";
        }
        better_hello("Hi", $name, "!");
        better_hello("Greeting's", "my Lord", "!!");

        better_hello("Greeting's", "my Lord", null);
	?>
</body>
</html>