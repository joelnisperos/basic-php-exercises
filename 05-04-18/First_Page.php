<!DOCTYPE html>
<html lang="en">
<head>
	<title>First Page</title>
</head>
<body>
    <?php
        $link_name = "Second Page";
        $id = 2;
        $company = "Johnsons & Johnsons";
    ?>

    <a href="Second_Page.php?id=<?php echo $id ?>&company=<?php echo rawurlencode($company); ?>">
        <?php echo $link_name ?>
    </a>
</body>
</html>